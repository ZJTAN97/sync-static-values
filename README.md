# Sync Static Values

PoC Experimentation to sync a bunch of values between Higher Ups and reliant applications.

## Gitlab CI Runner tips and pointer

-   add "&" at the end of the command to run in background, similar to "-d" in docker
-   "sleep" to sort of pause the runner
-   if stage name is the same, it will run parallel jobs

<br>

## Managing secrets

-   Repository Selected > Settings > CI/CD > Variables
-   Look for specific documentation w.r.g to the tool you are using on environmental variables naming convention

<br>

## Using cache

```

// can specify globally
cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
        - node_modules/


// or within jobs
sample:
    stage: ...
    cache:
        key: ${CI_COMMIT_REF_SLUG}
        paths:
            - node_modules/

```

<br>

## Defining variables

```

variables:
    example_1: value_1
    example_2: value_2

# To use

$example_1
$example_2

```

<br>

## before_script and after_script

-   can be declared to improve script readability

## to disabled jobs

-   put a "." infront of the job name

<br>

## Anchors in yml

```

base_person: &base
  city: Singapore
  location: Singapore

# person will extend base_person

person:
  <<: *base
  name: Test
  age: 10


```
